import React, { Component } from 'react'
import { inject, observer } from 'mobx-react'

import style from './Main.scss'

@inject('global')
@observer
class Main extends React.Component {
    state = {
    }

    render() {
        return (
            <p>Main Component</p>
        )
    }
}

export default Main
