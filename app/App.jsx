import React, { Component } from 'react'
import _ from 'lodash'

import { inject, observer } from 'mobx-react'

import css from './App.scss'

@inject('global', 'routing')
@observer
class App extends Component {
    state = {}

    render() {
        return (
            <div>
                {this.props.children}
            </div>
        )
    }
}

export default App
